﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {


	public static Ball instance = null; 
	public float speed = 5f;
	public static int lastSideTouched;
	// Use this for initialization

	void Awake() {
		if(instance == null) {
			instance = this;
		} else if(instance != this) {
			Destroy(this.gameObject);
		}
	}
	void Start () {
		Movement ();
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void SpeedChange() {
		speed += 5f;
	}

	void OnCollisionEnter(Collision other) {
		if (other.gameObject.name == "Bump1") {
			lastSideTouched = 1;
		} else if (other.gameObject.name == "Bump2") {
			lastSideTouched = 2;
		}
	}
	public void Movement() {
		float sx = Random.Range (0, 2) == 0 ? -1 : 1;
		float sy = Random.Range (0, 2) == 0 ? -1 : 1;
		//transform.position = new Vector3(0,1,0);

		GetComponent<Rigidbody> ().velocity = new Vector3 (speed * sx, speed * sy, 0f);
	}
}
