﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player_Move : MonoBehaviour {

  public GameObject go;
  public float defaultSpeed;
  private float newSpeed;

  public bool player1;
  public bool player2;

	// Use this for initialization
	void Start ()
  {
    GetSpeed((float)1.0);
	}
	
	// Update is called once per frame
	void Update ()
  {
    if (player1 == true)
    {
      if (Input.GetKey(KeyCode.W))
      {
        go.transform.Translate((Vector3.up) * newSpeed);
      }

      if (Input.GetKey(KeyCode.S))
      {
        go.transform.Translate((Vector3.down) * newSpeed);
      }
    }
    else
    {
      go.transform.Translate(Vector3.zero);
    }

    if (player2 == true)
    {
      if (Input.GetKey(KeyCode.UpArrow))
      {
        go.transform.Translate((Vector3.up) * newSpeed);
      }

      if (Input.GetKey(KeyCode.DownArrow))
      {
        go.transform.Translate((Vector3.down) * newSpeed);
      }
    }
    else
    {
      go.transform.Translate(Vector3.zero);
    }
  }

  public float GetSpeed(float multiplier)
  {
    newSpeed = defaultSpeed * multiplier;

    return newSpeed;
  }
}
